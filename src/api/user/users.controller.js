const bcrypt = require('bcryptjs');
const httpStatus = require('http-status');

// const ApiError = require('../../core/APIError');
const db = require('../../models/user');

const Login = async (req, res, next) => {
  try {
    const { email, password } = req.body;
    const user = await db.findOne( {email:email} );
    // if(email){
    //   user = await db.findOne( {email:email} );
    // }else{
    //   user = await db.findOne( {mobile:mobile} );
    // }
    if (!user) {
      // throw new ApiError({
      //   message: 'User does not exists',
      //   status: httpStatus.NOT_FOUND,
      // });
    }
  
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
    //   throw new ApiError({
    //         message: "Password Doesn't match",
    //         status: httpStatus.BAD_REQUEST,
    // })
    }
    user.password = null;
    return successResponse(res, {user});
  } catch (err) {
    next(err);
  }
};

const Register = async (req, res, next) => {
  try {
    const {
       first_name, last_name, email, password  
    } = req.body;
    const userExist = await db?.findOne({ email });
    if (userExist) {
      res.send(res,  'User already exists with same email')
    }
    const hash = await bcrypt.hash(password, 10);
    const user = await db.create({
      first_name,
      last_name,
      email,
      password: hash,
    });
    return successResponse(res, { user,});
  } catch (err) {
    next(err);
  }
};



module.exports = {
  Register,
  Login
};
