const http = require('http');
const app = require('./core/express');
const config = require('./config/env-vars');
require("./model/index");

// Debug logging
console.log('Config:', config);

// Create Http Server
const server = http.createServer(app);

server.listen(config.PORT);

server.on('listening', () => {
  console.log(`${config.NODE_ENV.toUpperCase()} Server is Listening on PORT ${config.PORT}`);
});

// Listen to error on listening to port
const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const bind = typeof port === 'string' ? `Pipe ${config.PORT}` : `Port ${config.PORT}`;
};
server.on('error', onError);

/**
 * Exports Express
 * @public
 */
module.exports = server;
