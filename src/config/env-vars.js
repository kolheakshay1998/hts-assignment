require('dotenv').config();

module.exports = {
  NODE_ENV: process.env.NODE_ENV || 'development',
  PORT: process.env.PORT || 8000,
};


const mongoose = require("mongoose");
// mongoose.set('strictQuery', false);
// mongoose.connect(`${process.env.DB}`, {useUnifiedTopology: true,useNewUrlParser: true } ,(err) => {
//   if(err){
//     console.log(err);
//   }
//   else
//     console.log("DB Connected");
// });

mongoose.set('strictQuery', false);

mongoose.connect(`${process.env.DB}`, {
  useUnifiedTopology: true,
  useNewUrlParser: true
})
.then(() => {
  console.log("DB Connected");
})
.catch((err) => {
  console.log(err);
});
